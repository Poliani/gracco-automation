import elements from '../elements/parte.processo.po';


Cypress.Commands.add("buscaProcesso", (selectProcesso) => {

    cy.get(selectProcesso)
      .select('Carteira').should('have.value', 'Carteira');

    cy.get(btnBuscaProcesso)
      .click();

  })


  Cypress.Commands.add("salvarParteProcesso", (selectPessoa, selectTipoParte) => {

    cy.get(selectPessoa)
      .select('Antonio da Silva Moraes').should('have.value', 'Antonio da Silva Moraes');

    cy.get(selectTipoParte)
      .select('Maria da Silva Moraes').should('have.value', 'Maria da Silva Moraes');
     
    cy.get(btnSalvarParte)
      .click();

  })
   
  Cypress.Commands.add("exportarProcesso", () => {

    cy.get(btnExportarProcesso)
      .click();
              
  })

  Cypress.Commands.add("historicoProcesso", () => {

    cy.get(btnHistoricoProcesso)
      .click();
              
  })