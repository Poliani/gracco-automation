const elements = {
    selectProcesso: '[data-test="select-processo"]',
    btnBuscaProcesso: '[data-test="button-busca-processo"]',
    selectPessoa: '[data-test="select-pessoa"]',
    selectTipoParte: '[data-test="select-tipo-parte"]',
    btnCancelarParte: '[data-test="button-cancelar-parte"]',
    btnSalvarParte: '[data-test="button-salvar-parte"]',
    btnExportarProcesso: '[data-test="button-exportar-processo"]',
    btnCompartilharProcesso: '[data-test="button-exportar-processo"]',
    btnHistoricoProcesso:  '[data-test="button-historico-processo"]',
    btnTutelarProcesso: '[data-test="button-tutelar-processo"]',
    btnEmailProcesso: '[data-test="button-email-processo"]',
    btnSalvarProcesso: '[data-test="button-salvar-processo"]',

};
export default elements;