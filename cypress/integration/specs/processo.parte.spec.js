/// <reference types="cypress" />

import {  } from "../../support/elements/parte.processo.po";

beforeEach(() => {
    cy.visit('https://gracco.com.br/admin');
    cy.get('[data-test="input-user_name"]').type(username);
    cy.get('[data-test="input-password"]').type(password);
    cy.get('[data-test="input-login"]').click();

})

it ('Busca por processo', () => {
    cy.buscaProcesso();        
})

it ('Busca por processo, adiciona Pessoa e tipo da parte e salvar informações', () => {
    cy.buscaProcesso(); 
    cy.salvarParteProcesso();
})

it ('Busca por processo e realizar a exportação', () => {
    cy.buscaProcesso(); 
    cy.exportarProcesso();
})

it ('Busca por processo e acessar histórico do processo', () => {
    cy.buscaProcesso(); 
    cy.historicoProcesso();
})




