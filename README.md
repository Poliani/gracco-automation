# Gracco-automation

Automação Gracco Test.




**Instalação**

**Faça o clone ou download do projeto.**

Acesse o diretório "Gracco-automation" e rode o comando:
  *npm install*
  
Ao terminar a instalação, rode o comando para iniciar o projeto:
  **npm run cypress:open**
  
Quando a aplicação estiver rodando acesse a janela do cypress. 

Esta janela abre de forma padrão na aba "Tests".
Acesse o caminho: "INTEGRATIONS TESTS > specs" e selecione qual deseja executar .